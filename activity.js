// Objective 1

db.rooms.insertOne({
    "name": "single",
    "accomodates": 2,
    "price": 1000,
    "description": "A simple room with all the basic necessities",
    "rooms_available": 10,
    "isAvailable": false
})


// Objective 2

db.rooms.insertMany([
    { 
        "name": "double",
        "accomodates": 3,
        "price": 2000,
        "description": "A room fit for a small family going on vacation",
        "rooms_available": 5,
        "isAvailable": false
    },
    {
        "name": "queen",
        "accomodates": 4,
        "price": 4000,
        "description": "A room with a queen sized bed perfect for a simple getaway",
        "rooms_available": 15,
        "isAvailable": false
    }
])

// Objective 3
db.rooms.find({"name": "double"})

// Objective 4
db.rooms.updateOne(
    {"_id": ObjectId("6419aefac05e967f9a7c0a0b")},
    { $set: {"rooms_available": 0}}
)

db.rooms.find({"name": "queen"})

// Objective 5
db.rooms.deleteMany({"rooms_available": 0})

db.rooms.find()
